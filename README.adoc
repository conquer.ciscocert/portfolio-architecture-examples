= Red Hat Portfolio Architecture Examples
Eric Schabell @eschabell, Marty Wesley @mwesley_redhat, William Henry @ipbabble, Will Nix @tronik, Ishu Verma  @ishuverma, Marcos Entenza @makentenza
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify

== Overview
This repository provides examples of customer researched based general architectures based on usage of the Red Hat
product portfolio. All architectural diagrams are created using the open source tool draw.io. The examples provide for
*Logical* diagrams, more in-depth view of how the solution works in *Schematic* diagrams and closer looks at
elements in *Detailed* diagrams.

TIP: Learn how to use the draw.io tool to create portfolio architecture diagrams with https://gitlab.com/redhatdemocentral/portfolio-architecture-workshops[Portfolio Architecture Workshop]

TIP: Learn how to develop your own blueprint project and add to this examples repository with https://redhatdemocentral.gitlab.io/portfolio-architecture-template[Beginners guide to deveoping an architecture blueprint]

== Portfolio Architecture project index
[cols="3,7"]
|===
|Title | Description

|link:businessoptimization.adoc[Business optimisation]
|Optimizing delivery routing, automating rostering of staff, and improving efficiency of tasks across multiple stores.

|link:cloud-adoption.adoc[Cloud adoption]
|Accelerating cloud adoption with effective automation for deploying and managing workloads across multiple cloud
infrastructures according to performance, security, compliance, and cost requirements.

|link:cloud-factory.adoc[Cloud factory]
|Deploy multiple private clouds based on the same (infrastructure as) code using different parametrization (Ansible
inventories).

|link:cnd.adoc[Cloud native development]
|Cloud native development is an approach to building and running applications to fully exploit the advantages of the
cloud computing model (i.e. responsive, elastic and resilient applications).

|link:edge-cloud.adoc[Cloud to edge]
|Providing a consistent infrastructure experience from cloud to edge and enabling modern containerized applications at edge.

|link:datacenter-to-edge.adoc[Data center to edge]
|Bringing computing closer to the edge by monitoring for potential issues with gas pipeline (edge).

|link:edge.adoc[Edge]
|This is a *collection of various architectures* for edge computing. Customers are increasing looking to edge
computing to improve operational efficiencies, enhance end-user experiences, meet data residency requirements, and run
resilient, semi-autonomous applications.

|link:edge-manufacturing-efficency.adoc[Edge manufacturing efficiency]
|Boosting manufacturing efficiency and product quality with artificial intelligence and machine learning out to the edge.

|link:edge-medical-diagnosis.adoc[Edge medical diagnosis]
|Accelerating medical diagnosis using condition detection in medical imagery with AI/ML at medical facilities.

|link:finance.adoc[Finance]
|This is a *collection of various architectures* in the financial services domain. Financial services institutions
understand that today’s banking customers expect fast, easy-to-use services they can tap into anytime, anywhere,
and are therefore accelerating adoption of digital technologies to enable a variety of new offerings.

|link:headlessecommerce.adoc[Headless e-commerce]
|Deploying a container based eCommerce website while moving away from tightly coupled existing eCommerce platform.

|link:healthcare.adoc[Healthcare]
|This is a *collection of various architectures* in the healthcare domain. Our world transformed overnight, and
healthcare had to respond immediately. Now more than ever, it's critical to have access to data and unlimited scale,
and to design and modify decisioning, processes, and workflows for specific situations—without being vendor-dependent.

|link:spi-multi-cloud-gitops.adoc[Hybrid multicloud management with GitOps]
|Manage hybrid and multicloud cluster lifecycle, secure configuration and credentials. GitOps for Infrastructure as
code approach. Cross cluster governance and application lifecycle management.

|link:integrated-saas.adoc[Integrating with SaaS applications]
|Providing integration with SaaS applications, platforms, and services empowers organizations to build and run scalable
applications in modern, dynamic environments such as public, private, and hybrid clouds.

|link:idaas.adoc[Intelligent data as a service (iDaaS)]
|Intelligent DaaS (Data as a Service) is about building and delivery of systems and platforms in a secure and scalable
manner while driving data needs for moving towards consumerisation in healthcare.

|link:manufacturing.adoc[Manufacturing]
|This is a *collection of various architectures* in the retail domain. The manufacturing industry has consistently used
technology to fuel innovation, production optimization and operations. Now, with the combination of edge computing and
AI/ML, manufacturers can benefit from bringing processing power closer to data. This helps actions be taken faster on
things like errors and predictive maintenance.

|link:data-synthesis.adoc[Modernising testing with data synthesis]
|Modernising testing with data synthesis is a testing innovation with focus around enabling massive amounts of data to
be available for both business users and developers in a secure and scalable manner.

|link:nzd-sap.adoc[Near Zero Downtime Maintenance for SAP]
|Minimizing the downtime of the maintenances on SAP hosts so that users and processes can continue to work
without perceiving any interruption.

|link:omnichannel.adoc[Omnichannel customer experience]
|Omnichannel implies integration and orchestration of channels such that the experience of engaging across all the
channels someone chooses to use.

|link:openbanking.adoc[Open banking]
|A cloud ready, modular open source approach offering a wide range of technology options and allows
low-effort integration covering all aspects of an Open Banking implementation.

|link:payments.adoc[Payments]
|Financial institutions enabling customers with fast, easy to use, and safe payment services available anytime, anywhere.

|link:pointofsale.adoc[Point of sale]
| Simplifying and modernizing central management of distributed point-of-sale devices with built in support for
container based applications.

|link:realtimestock.adoc[Real-time stock control]
|Providing (near) real-time stock positions and dynamic pricing promotions information to retailer omnichannels.

|link:remote-management.adoc[Remote server management]
|Providing remote management for a consist server estate across hybrid cloud and data centers with remote management,
security, and  data protection for full lifecycle.

|link:retail.adoc[Retail]
|This is a *collection of various architectures* in the retail domain. Retail is the process of selling consumer
goods or services to customers through multiple channels of distribution to earn a profit. Retailers satisfy demand
identified through a supply chain. The term "retailer" is typically applied where a service provider fills the small
orders of many individuals, who are end-users, rather than large orders of a small number of wholesale, corporate or
government clientele.

|link:retaildataframework.adoc[Retail data framework]
|Creating a framework for access to retail data from customers, stock, stores, and staff across multiple internal teams.

|link:scada-interface.adoc[SCADA interface modernisation]
|Provide interfaces with SCADA systems that are compliant with NERC regulations, creating different layers of API
gateways to protect business service depending on the network zones.

|link:self-healing.adoc[Self-Healing Infrastructure]
|Managing security, policy and patches for a large number of servers in data centers or public/private clouds.

|link:sap-smart-management.adoc[Smart management for SAP]
|Managing security, policy and patches for all the servers in the SAP ecosystem (on-premise, public, private and
hybrid cloud), making sure they are compliant with SAP and Red Hat's recommendations through their entire lifecycle.

|link:storehealthandsafety.adoc[Store health and safety]
|Managing effective in-store compliance, health & safety, and employee checks and procedures.

|link:supplychainintegration.adoc[Supply chain integration]
|Streamlining integration between different elements of a retail supply chain for on-premise, cloud, and other
third-party interactions.

|link:telco-on-premise.adoc[Telco 5G core: on-premise]
|Ultra-reliable, immersive experiences for people and objects when and where it matters most.

|link:telco-5g-with-hyperscalers.adoc[Telco 5G with hyperscalers]
|Build an adaptable, on-demand infrastructure services for 5G Core that can deliver across diverse use cases with
minimal CAPEX and OPEX.

|link:telco-radio-access-networks.adoc[Telco radio access networks]
|The digital transformation of mobile networks  is accelerating and cloudification is increasing. Following the core
network, radio access network (RAN) solutions are now taking advantage of the benefits of cloud computing.

|link:telco.adoc[Telco]
|This is a *collection of various architectures* in the telco domain. Some of the telco use cases cover broadband
technology evolution like 5G while others cover infrastructure modernization like Radio Access Network.

|link:utility.adoc[Utility]
|This is a *collection of various architectures* in the utility domain.The energy (utility) infrastructure companies
operate across vast geographical area that connects the upstream drilling operations with downstream fuel processing
and delivery to customers. These companies need to monitor the condition of pipeline and other infrastructure for
operational safety and optimization.
|===
